from pycorenlp import StanfordCoreNLP
import sqlite3

DATABASE_URL = "../corpus.db"
LAW_TABLE = "law_text"
PARSE_TABLE = "parse_trees"

nlp = StanfordCoreNLP('http://localhost:9000')


def create_db():
    conn = sqlite3.connect(DATABASE_URL)
    c = conn.cursor()
    #TODO Warning: law_text table name is hardcode in!
    c.execute('''CREATE TABLE %s ( `law_text_id` INTEGER NOT NULL,  `tree_num` INTEGER NOT NULL, `parse_tree` BLOB, FOREIGN KEY(`law_text_id`) REFERENCES law_text(id) )''' % PARSE_TABLE)
    conn.commit()
    conn.close()


def result_iter(cursor, arraysize=1000):
    'An iterator that uses fetchmany to keep memory usage down'
    while True:
        results = cursor.fetchmany(arraysize)
        if not results:
            break
        for result in results:
            #First element is the rowid and the last is law_text
            yield (result[0], result[-1])


def write_to_db(write_conn, law_id, tree_num, text):
    c = write_conn.cursor()
    t = (law_id, tree_num, text)
    c.execute("INSERT INTO %s VALUES (?,?,?)" % PARSE_TABLE, t)
    write_conn.commit()


def parse_rows(read_conn):
    c = read_conn.cursor()
    c.execute("SELECT rowid, * from %s" % LAW_TABLE)
    for row_id, law_sent in result_iter(c, 500):
        sub_row = 0
        parse_trees = get_parse_tree(law_sent)
        if not parse_trees:
            continue
        for p in parse_trees:
            write_to_db(read_conn, row_id, sub_row, p)
            print("Writing: ", row_id)
            sub_row += 1


def get_parse_tree(text):
    try:
        outputs = nlp.annotate(text, properties={
          'annotators': 'parse',
          'outputFormat': 'json'
          })['sentences']
    except:
        print("ERROR: CoreNLP timeout")
        return None
    parse_trees = []
    for sent in outputs:
        parse_trees.append(sent['parse'])
    return parse_trees


if __name__ == "__main__":
    create_db()
    conn = sqlite3.connect("../corpus.db")
    parse_rows(conn)
    conn.close()

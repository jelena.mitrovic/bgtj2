# coding: utf-8
def ResultIter(cursor, arraysize=1000):
    'An iterator that uses fetchmany to keep memory usage down'
    while True:
        results = cursor.fetchmany(arraysize)
        if not results:
            break
        for result in results:
            yield result
            
import sqlite3
conn = sqlite3.connect("../corpus.db")
c= conn.cursor()
c.execute("SELECT * from law_db")
c.execute("SELECT * from laws")
c.execute("SELECT * from %s" % "law_text")
for row in ResultIter(c, 500):
    print(row)
    input()
    
get_ipython().magic('save scratchbook 0/')

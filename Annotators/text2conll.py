from pycorenlp import StanfordCoreNLP
import sqlite3

DATABASE_URL = "../corpus.db"
LAW_TABLE = "law_text"
CONLL_TABLE = "conll"

import requests
POST_URL = "http://0.0.0.0:7000/v1/parsey-universal-full"
HEADERS = { "Content-Language" : "en"}

def create_db():
    conn = sqlite3.connect(DATABASE_URL)
    c = conn.cursor()
    #TODO Warning: law_text table name is hardcode in!
    c.execute('''CREATE TABLE %s ( `law_text_id` INTEGER NOT NULL,  `conll` BLOB, FOREIGN KEY(`law_text_id`) REFERENCES law_text(id) )''' % CONLL_TABLE)
    conn.commit()
    conn.close()

def result_iter(cursor, arraysize=1000):
    'An iterator that uses fetchmany to keep memory usage down'
    while True:
        results = cursor.fetchmany(arraysize)
        if not results:
            break
        for result in results:
            #First element is the rowid and the last is law_text
            # yield (result[0], result[-1])
            
            #Now it's the second element! (because of the sql query filtering only stgb
            yield (result[1]+"."+str(result[2]), result[-1])

def write_to_db(write_conn, law_id, text):
    c = write_conn.cursor()
    t = (law_id, text)
    c.execute("INSERT INTO %s VALUES (?,?)" % CONLL_TABLE, t)
    write_conn.commit()



def run_batch(read_conn):
    c= read_conn.cursor()
    # All sentences
    # c.execute("SELECT rowid, * from %s" % LAW_TABLE)
    # Only stgb
    c.execute('SELECT * from %s where "law_name" = "stgb"' % LAW_TABLE)
    for row_id, law_sent in result_iter(c, 500):
        conll = get_conll(law_sent)
        if not conll:
            continue
        write_to_db(read_conn, row_id, conll)
        print("Writing: ", row_id)


def get_conll(text):
    r = requests.post(POST_URL, data=text.encode("utf-8"), headers = HEADERS)
    try:
       return r.text
    except:
        print("ERROR: DRAGNN timeout")
        return None


if __name__ == "__main__":
    create_db()
    conn = sqlite3.connect("../corpus.db")
    run_batch(conn)
    conn.close()

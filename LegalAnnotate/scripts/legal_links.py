import re
import spacy
# from pdb import set_trace

links = []
tokenizer_nlp = spacy.load('en', disable=['tagger', 'ner', 'parser'])
t_nlp = tokenizer_nlp


def encode_links(row, matches):
    TAG_START = "\33[32m{"
    TAG_END = '}\033[0m'
    law_section, row_paragraph, p_number, p_name, law_text = row
    # matches = [m[0] for m in links]

    # Matched text which needs to be replaced is sorted in
    # The reverse order
    if matches == []:
        return law_text

    matches = sorted(set(matches), key=len, reverse=True)
    section_finder = re.compile(r"""(
                                       # of the German Criminal Code
        # Remove \w from here!
        (of\sthe\s([A-Z][a-z\-]+\s?)+(Act|Code))
        |
                                       # of the Copyright Act
        (\sof\sthe\sCode\sof(\s[A-Z][a-z\-]+)+)
        |
                                       # , Copyright Act
        (,(\s[A-Z][a-z]+)+\sAct)
                                       # the Act on Proceedings in Family...
                                       # Matters and in Matters of...
                                       # Non-Contentious Jurisdiction
        |
        (\sthe\sAct\son\s(((and|in|of)\s)*[A-Z][a-z]+(\s|\-))+)
    )""", re.X)

    section = section_finder.search(law_text)
    if section:
        section = section.group()
        # print("My section is", section)
    else:
        # if a section is not mentioned; use current section
        section = law_section
    # new_text = ""
    for m in matches:
        formatted_link = TAG_START + m.replace(" ", ":") + TAG_END
        law_text = law_text.replace(m, formatted_link)
        # print("="*80)
    return law_text


# [(law tuple)] -> [(law tuple with text embedded links)]
def extract_links(simple_laws_rows):
    link_finder = re.compile(r"""(
        [Ss]ections?\s                 # Section | sections

        (                              # `,`, to, and , or, `, and `, through
        (,\s?|\sto\s|\sor\s|,?\sand\s|\sthrough\s)?

        \d+\w?                         # 11a

        (
        (,|,?\sand|\sto)?              # `,`, and, to, `, and`
        (
        (\ssubsections?)?              # subsection
        \s?\(\d+\)                     # (11)
        |
                                       # , 2nd sentence | second sentence
        \s(\w+|\d+(st|rd|th))\ssentence
        |
        \snumbers?\s\d+                # number 1 | numbers 20
        |
        \spara.?\s\d+(\sclause\s\d)?   # para. 9 clause 2
        )
        (\sas\swell\sas)?
        )*

        (,?\s[Nn]o.?\s\d+)*            # , No. 2 | no 2

        (
                                       # of the German Criminal Code
        # Remove \w from here!
        ((\s\w+\s([\w ]+)?)?\sof\sthe\s[\w\- ]+(Act|Code))
        |
                                       # of the Copyright Act
        (\sof\sthe\sCode\sof(\s[A-Z][a-z\-]+)+)
        |
                                       # , Copyright Act
        (,(\s[A-Z][a-z]+)+\sAct)
                                       # the Act on Proceedings in Family...
                                       # Matters and in Matters of...
                                       # Non-Contentious Jurisdiction
        |
        (\sthe\sAct\son\s(((and|in|of)\s)*[A-Z][a-z]+(\s|\-))+)
        )?
        )+
    )""", re.X)

    laws_with_links = []

    for row in simple_laws_rows:
        law_name, section, p_number, p_name, law_text = row
        links = link_finder.findall(law_text)
        if not links:
            continue
        # Getting only the useful parts
        links = [l[0] for l in links]
        # new_text = encode_links(row, links)
        new_row = (law_name, section, p_number, p_name, law_text, links)
        # print(row, end="\n"+"-"*20+"\n")
        # print(links)
        # print(new_row[-1], end="\n"+"-"*20+"\n\n")
        laws_with_links.append(new_row)
    return laws_with_links


# [(law tuple)] -> [[bounds]]
def get_bounds(law_text):
    link_finder = re.compile(r"""(
        [Ss]ections?\s                 # Section | sections

        (                              # `,`, to, and , or, `, and `, through
        (,\s?|\sto\s|\sor\s|,?\sand\s|\sthrough\s)?

        \d+\w?                         # 11a

        (
        (,|,?\sand|\sto)?              # `,`, and, to, `, and`
        (
        (\ssubsections?)?              # subsection
        \s?\(\d+\)                     # (11)
        |
                                       # , 2nd sentence | second sentence
        \s(\w+|\d+(st|rd|th))\ssentence
        |
        \snumbers?\s\d+                # number 1 | numbers 20
        |
        \spara.?\s\d+(\sclause\s\d)?   # para. 9 clause 2
        )
        (\sas\swell\sas)?
        )*

        (,?\s[Nn]o.?\s\d+)*            # , No. 2 | no 2

        (
                                       # of the German Criminal Code
        # Remove \w from here!
        ((\s\w+\s([\w ]+)?)?\sof\sthe\s[\w\- ]+(Act|Code))
        |
                                       # of the Copyright Act
        (\sof\sthe\sCode\sof(\s[A-Z][a-z\-]+)+)
        |
                                       # , Copyright Act
        (,(\s[A-Z][a-z]+)+\sAct)
                                       # the Act on Proceedings in Family...
                                       # Matters and in Matters of...
                                       # Non-Contentious Jurisdiction
        |
        (\sthe\sAct\son\s(((and|in|of)\s)*[A-Z][a-z]+(\s|\-))+)
        )?
        )+
    )""", re.X)
    links = link_finder.findall(law_text)
    links = [l[0] for l in links]
    if not links:
        return []
    bounds = []
    for l in links:
        start = law_text.index(l)
        end = start + len(l)
        bound = (start, end)
        bounds.append(bound)
    return bounds


def link_merge(doc):
    """
    Custom spacy component used to merge legal link
    token to single tokens
    """
    # We get charecter span positions below
    bounds = get_bounds(doc.text)
    offset = 0
    for x, y in bounds:
        # We have to translate it to token positions
        tok_start = tokenizer_nlp(doc.text[:x])
        tok_link = tokenizer_nlp(doc.text[x:y])
        start = len(tok_start) - offset
        end = start + len(tok_link)
        span = doc[start:end]
        offset += len(span) - 1
        # print(doc, end="\n"+"-"*50 + "\n")
        # print(f"{span} @ {start}:{end}/{len(doc)}\n\n")
        # pdb.set_trace()
        span.merge()
    return doc


def run():
    import legaltext
    laws_rows = legaltext.get_section("stgb")
    laws_with_links = extract_links(laws_rows)
    # new_law_text = encode_links(laws_with_links)
    # for l in new_law_text:
    #    print(l[4], end="\n\n")
    # picke_file = open("simple_laws_links.pickle", 'wb')
    # pickle.dump(simple_laws_with_links, picke_file)
    links = []
    for _, _, _, _, _, link in laws_with_links:
        links.extend(link)
    from pprint import pprint
    pprint(links)
    import pickle
    pickle.dump(links, open("stgb_links.pickle", "wb"))


if __name__ == "__main__":
    run()

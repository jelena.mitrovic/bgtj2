from legal_text_framer import get_frames
from spacy_pipeline import nlp as en_nlp

sentences = get_frames(en_nlp, "Whosoever, in order to deceive a public authority or an insurance company about his own state of health or that of another, makes use of a certificate of the type indicated in section 277 and section 278, shall be liable to imprisonment not exceeding one year or a fine.")

# def make_token(obj, root):
#     frames = []
#     for c in obj.children:
#         loc = c.i
#         subtree = list(c.subtree)
#         subphrase = ""
#         first = True
#         for node in subtree:
#             token = f"{node.orth_}"
#             subphrase += " "+token
#         header = (c, c.pos_, loc)
#         my_frame = (header, subphrase)
#         frames.append(my_frame)
#     return frames

    
for s in sentences:
    for f in s:
        head, h_text = f
        obj, h_pos, h_loc = head
        print(h_text)
        # print(list(c.subtree))
        sub_frames = []
        subtree_num = len(list(obj.children))
        if subtree_num < 3:
            continue
        # make_token(obj, root)
        # print(frames)
        for c in obj.children:
            loc = c.i
            subtree = list(c.subtree)
            subphrase = ""
            first = True
            for node in subtree:
                token = f"{node.orth_}"
                subphrase += " "+token
            header = (c, c.pos_, loc)
            my_frame = (header, subphrase)
            sub_frames.append(my_frame)
            print(my_frame)
        

# print(frames)

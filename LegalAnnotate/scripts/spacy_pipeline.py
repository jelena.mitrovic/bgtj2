import spacy
import legaltext
from legal_links import link_merge

nlp = spacy.load('en')
nlp.add_pipe(link_merge, name='link_merge', before='tagger')

if __name__ == "__main__":
    paras = legaltext.get_section("stgb")
    for para in paras[12:]:
        law_section, row_paragraph, p_number, p_name, law_text = para
        doc = nlp(law_text)
        print(list(doc), end="\n\n")

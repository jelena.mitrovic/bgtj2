from legal_text_framer import get_frames
from spacy_pipeline import nlp as en_nlp

def get_labels(nlp, frames):
    labelled = []
    for f in frames:
        print(f)
    return labelled

frames = get_frames(en_nlp, "Whosoever, in order to deceive a public authority or an insurance company about his own state of health or that of another, makes use of a certificate of the type indicated in section 277 and section 278, shall be liable to imprisonment not exceeding one year or a fine.")
get_labels(en_nlp, frames)

legal_result = ("contains", "shall be liable to")
bob = ("lower", "whosoever")
circumstance = ("pos", "ADP")
act = ("pos", "ACT_VERB") # the root verb, find the text with #_ in the beginning
conditional = ("contains", "if")

rules = {"legal_result":legal_result, "bob":bob, "circumstance":circumstance,
        "act": act, "conditional":conditional}

def match_contains(token, pattern):
    print(token, pattern)
    _, text = token

    if pattern in text.lower():
        return True
    else:
        return False

def match_lower(token, pattern):
    _, text = token
    if pattern == text.lower():
        return True
    else:
        return False

def match_pos(token, pattern):
    meta, text = token
    root, pos, loc = meta
    if pos == pattern:
        return True
    else:
        return False

legal_result = (match_contains, "shall be liable to")
bob = (match_lower, "whosoever")
circumstance = (match_pos, "ADP")
act = (match_pos, "ACT_VERB") # the root verb, find the text with #_ in the beginning
conditional = (match_contains, "if")

rules = {"legal_result":legal_result, "bob":bob, "circumstance":circumstance,
        "act": act, "conditional":conditional}

# def match_frames(frames, rules):
#     frame_types = []
#     for f in frames:
#         rkeys = rules.keys()
#         for k in rkeys:

frames = get_frames(en_nlp, "Whosoever, in order to deceive a public authority or an insurance company about his own state of health or that of another, makes use of a certificate of the type indicated in section 277 and section 278, shall be liable to imprisonment not exceeding one year or a fine.")

def annotate(sentences, rules):
    annotations = []
    for frames in sentences:
        for f in frames:
            none = True
            for r in rules.keys():
                func, arg = rules[r]
                if func(f, arg):
                    annotations.append(r)
                    none = False
                break
            if none:
                annotations.append("NONE")
    return annotations
#match_frames(frames, rules)

# f = rules["act"][0]
# arg = rules["act"][1]

# print(f((("makes", 'ACT_VERB', 24), "makes"),arg))

print(annotate(frames, rules))

import sqlite3
import logging

database_path = "../../Databases/corpus_en.db"


# def only_get_text(legal):


def get_all():
    conn = sqlite3.connect(database_path)
    c = conn.cursor()
    table_name = "law_text"
    c.execute(f"select * from {table_name};")
    all_legal_text = c.fetchall()
    conn.close()
    logging.info(f"There are {len(all_legal_text)}"
                 " number of legal text instances")
    return all_legal_text


def get_section(section):
    section_text = [
        (code, para, sub_para, para_id, text)
        for code, para, sub_para, para_id, text in get_all()
        if code == section
    ]
    return section_text


def get_para(section, target_para_id):
    para_text = [
        (code, para, sub_para, para_id, text)
        for code, para, sub_para, para_id, text in get_all()
        if section == code and target_para_id == para_id
    ][0]
    return para_text


if __name__ == "__main__":
    stgb_paragraphs = get_section("stgb")
    print(f"There are {len(stgb_paragraphs)} entities in stgb")
    print(stgb_paragraphs[0])
    single_tuple = get_para("stgb", "p0582")
    print(single_tuple)

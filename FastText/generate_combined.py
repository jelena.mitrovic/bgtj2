# coding: utf-8
import sqlite3
conn = sqlite3.connect("../corpus.db")
c = conn.cursor()
query = c.execute("select law_text from law_text")
f = open("combined.txt", "a")
text =  query.fetchone()
while text:
    f.write(text[0]+"\n")
    text = query.fetchone()
f.close()

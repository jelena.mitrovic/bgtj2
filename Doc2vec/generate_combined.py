# coding: utf-8
from gensim.models.doc2vec import TaggedDocument
import sqlite3
import pickle


def read_db():
    conn = sqlite3.connect("../corpus.db")
    c = conn.cursor()
    query = c.execute("select * from law_text")
    # ~ f = open("combined.txt", "a")
    return query.fetchall()


def to_ignore(s:str):
    """
    The first word is sent. 
    If the word should be ignore, True is returned.
    """
    # Ignore (1) in "(1) “Collecting society”
    # means an organisation which is
    # authorised by law"
    if s[0] == '(':
        return True
    # Ignore 1. in "1. The first point...."
    # Ignore b) in "b) a report on activities"
    if s[-1] == '.' or s[-1] == ')':
        return True
    return False


if __name__ == "__main__":
    docs = []
    for row in read_db():
        section, para_num, sub_para_num, \
            text = row
        text_split = text.split()
        if len(text_split) == 0:
            continue
        if to_ignore(text_split[0]):
            words = text_split[1:]
        else:
            words = text_split
        tags = [section, section+"."+para_num]
        print(tags[1])
        new_doc = TaggedDocument(words, tags)
        docs.append(new_doc)
    pickle_file = open("docs.pickle", "wb")
    pickle.dump(docs, pickle_file)
    pickle_file.close()


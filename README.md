# BGTJ2
# A semnantic law interpreter
BGTJ2 is the ground up rebuild of a semantic law interpreter,
 done the "right" way.

Major modules of the system include:

1. Manual annotation + support
2. automatic annotation using NLP tools
3. Pseudo-Logical form generator
4. Judging program

<!-- ## Status -->
<!-- *  -->
<!-- ## Complete tasks: -->

<!-- ### For English text -->
<!-- * Download of entire law corpus -->
<!-- * Extraction of the law texts -->
<!-- * Splitting of text to points -->
<!-- * Added english law texts to a database -->

import os
import codecs
import re

PATH_HTML_FILES = "../Corpus/gesetze_en/source_html/"
SOURCE_ENCODING = "iso-8859-1"
DATABASE_URL = "../Databases/corpus_en.db"
TABLE_NAME = "law_titles"


def create_db():
    import sqlite3
    conn = sqlite3.connect(DATABASE_URL)
    c = conn.cursor()
    c.execute('''CREATE TABLE %s (law_name text, section text, law_title text)''' % TABLE_NAME)
    conn.commit()
    conn.close()


def write_to_db(law_name, section_number, law_title):
    import sqlite3
    conn = sqlite3.connect(DATABASE_URL)
    c = conn.cursor()
    t = (law_name, section_number, law_title)
    c.execute("INSERT INTO %s VALUES (?,?,?)" % TABLE_NAME, t)
    conn.commit()
    conn.close()


def read_file(html_file="../gesetze/html/freiz.html"):
    """
    Inputs:
        html_file: File path of html file
    Output:
        Normalized unicode text of the html file
    """
    f = codecs.open(html_file, "rb", SOURCE_ENCODING)
    file_text = f.read()
    import unicodedata
    return unicodedata.normalize("NFKD", file_text)


def yield_p(html_file):
    """
    Inputs: 
        html_file: A HTML file (from the Gestze) in the form of a string

    Output: 
        An iterator providing every p element's text
    """
    from bs4 import BeautifulSoup
    html_file = read_file(html_file)
    soup = BeautifulSoup(html_file, 'html.parser')
    text_in_container = soup.find(id="paddingLR12")
    p_parts = text_in_container.find_all("p", recursive=False)

    for p in p_parts:
        text = p.get_text()
        if text is "":
            continue
        if text == "table of contents":
            continue
        yield p


def write_file(law_name, section_number, sub_section, p):
    if not os.path.exists("splittext/" + law_name):
        os.makedirs("splittext/" + law_name)

    if True:
        f = codecs.open("splittext/" + law_name + "/" + \
                        section_number + "." + str(sub_section), "w", "utf-8")
        f.write(p)
        f.close()


def run(file_path, file_name):
    """
    Inputs:
        file_path: The path of the input HTML file
        file_name: Is the file_name / official (as used in the URLs) acronym of 
                the law ordinance

    Outputs:
        None, but causes a side effect

    Side effect:
        Writes text files with the splittext files
    """
    sections = []
    titles = []
    current_section = ""

    redundant = True
    for p in yield_p(file_path):
        # Check if the p is a heading or is law text
        if (p.has_attr('style')):
            for br in p.find_all("br"):
                br.replace_with("\n")
            # Some DEBUG checks to print exceptions to the next if
            if "section" in p.get_text() and not ("Chapter" in p.get_text()):
                if (not re.search("\Asection[s]? ", p.get_text().lower()) or \
                            re.search("\A§ ", p.get_text())):
                    print('Error at:')
                    print("\nLaw: ", file_name)
                    print(p.get_text())
                    continue

            if (re.search("\Asection[s]? ", p.get_text().lower()) \
                        or re.search("\A§ ", p.get_text())):
                # Since the section has started, there is nothing to ignore
                redundant = False
                if not ("\n" in p.get_text()):
                    number = p.get_text().split(" ")[1]
                    title = ''
                    # title = p.get_text().split("\n")[1]
                    # print('title: ', p.get_text())
                else:
                    number, title = p.get_text().split("\n", 1)
                    print('title: ', file_name, number, title)
                    input()
                    number = number.split(" ")[-1]
                current_section = number
                sections.append(number)
                titles.append(title)
                sub_section = 0
            continue

        # Ignore the text before the start of the first section
        if (redundant):
            continue

        # What is this for?!
        # if(p.get_text()[0:5] == "Part "):
        # continue

        # write_file(file_name, current_section, sub_section,\
        #   p.get_text().strip())


if __name__ == "__main__":
    # create_db()
    from os import listdir

    files = listdir(path=PATH_HTML_FILES)
    for f in files:
        run(PATH_HTML_FILES + f, f.split(".")[-2])

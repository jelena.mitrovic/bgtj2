"""
This module is only useful for downloading the english version of the corpus
"""

# coding: utf-8

DOWNLOAD_FOLDER = "../Corpus/gesetze_en/html/"
from bs4 import BeautifulSoup
import urllib.request as urlrequest


online_address = "http://www.gesetze-im-internet.de/Teilliste_translations.html"
webpage = urlrequest.urlopen(online_address).read()
soup = BeautifulSoup(webpage, "lxml")
tag_abbr = soup.find_all("abbr")

#now we have the ordinances of the German law
links = []
base_link = "http://www.gesetze-im-internet.de/"

for a in soup.find_all('a'):
    if(a.find_all("abbr")):
        links.append(a['href'])
        
    
for link in links:
    part_url = link.split("/")[0]
    url = base_link + part_url + "/" + part_url + ".html"
    print("Fetching: ", url)
    webpage = urlrequest.urlopen(url).read()
    title = part_url.split("_")[1]
    html_file = open(DOWNLOAD_FOLDER + title + ".html", "wb")
    html_file.write(webpage)
    html_file.close()


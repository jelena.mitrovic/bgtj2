from flask import Flask, render_template, request
from search import search
from law_analyzers import get_analysis

application = Flask(__name__)


@application.route("/")
@application.route("/search")
def index():
    return render_template("index.html")


@application.route("/query", methods=['POST'])
def query():
    query = request.form['query']
    # ~ langauge = request.form['langauge']
    langauge = 'en'
    response = search(query)
    #return results
    return render_template("search.html", response = response, query=query, language = langauge)


@application.route("/query2", methods=['POST'])
def query2():
    query = request.form['query']
    # ~ langauge = request.form['langauge']
    langauge = 'en'
    response = search(query)
    #return results
    return render_template("search2.html", response = response, query=query, language = langauge)


@application.route("/analysis/<law_id>")
def _analysis(law_id):
    lawtitle, paran = law_id.split('-')
    analysis = get_analysis(lawtitle, paran)
    return render_template("analysis.html", law_title=lawtitle, para_n=paran, analysis=analysis)


if __name__ == '__main__':
    application.run(debug=True)

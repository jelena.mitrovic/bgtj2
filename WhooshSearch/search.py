"""
Use Whoosh index files to find law section
"""
from whoosh.qparser import QueryParser
from whoosh.analysis import RegexTokenizer
from whoosh.analysis import StopFilter
import whoosh.index as index
from query_processor import query2lawsims, query2lawsims_or, query2lawsyns
from pprint import pprint


INDEX_EN = '../Corpus/gesetze_en/whoosh_index'
INDEX_TITLES_EN = '../Corpus/gesetze_en/whoosh_titles_index'


def search_runner(tokens, index_dir, preprocessor, content_tag, score_mult):
    """
    Wrapper to the whoosh search
    :param tokens: Input tokens
    :param index: Whoosh index directory
    :param combination_logic: OR logic or NOT ( True for OR, False for AND (?))
    :param content_tag: Tag wrapper for content, eg. H4 or P
    :param score_mult: Multiply score with a factor
    :return: results list - a list of results. A result is a list containing: content, law_title, para_n, score, url
    """
    ix = index.open_dir(index_dir)
    query_string = preprocessor(tokens)
    results_list = []
    with ix.searcher() as searcher:
        query = QueryParser("content", ix.schema).parse(query_string)
        results = searcher.search(query)
        if results:
            for r in results:
                print(r)
                content = f'<{content_tag}>' + r['content'] + f'</{content_tag}>'
                law_title = r['law_title']
                para_n = r['para_n']
                p_number = r['p_number']
                if not para_n:
                    para_n = None
                score = r.score * score_mult
                url = f'http://www.gesetze-im-internet.de/englisch_{law_title}/englisch_{law_title}.html#{p_number}'
                results_list.append([content, law_title, para_n, score, url])
    return results_list


def query_passthrough(tokens):
    return ' '.join(tokens)


# def query_generator_or(tokens):
#     return query_to_lawsims_query(tokens, force_or=True)
#
# def search_law_text_syns(tokens):
#     results_list = search_runner(tokens, INDEX_EN, query_to_lawsims, 'span', 2)
#     return results_list


def post_process(results_list):
    from pprint import pprint
    law_sections = {}
    # for i, r in enumerate(results_list):
    #     print(i,r)
    # print('\n'*5)
    for i, result in enumerate(results_list):
        content_text, law_title, para_n, score, url = result
        if len(content_text) > 300:
            content_text = content_text[:140]+"..."
        law_id = (law_title, para_n)
        if law_id in law_sections.keys():
            # Elements with _ are older than the one without
            content_list, _law_title, _para_n, _score, _url = law_sections[law_id]
            content_list.append(content_text)
            score = score + _score
            law_sections[law_id] = [content_list, law_title, para_n, score, _url]
            pprint(law_sections)
            print('\n')
        else:
            law_sections[law_id] = [[content_text], law_title, para_n, score, url]
    for r in law_sections.keys():
        content = law_sections[r][0]
        content = set(content)
        # Content with heading come before other...
        content = sorted(list(content), key=lambda x: 0 if '<H' in x else 1)
        law_sections[r][0] = content
    processed_list = [val for val in law_sections.values()]
    processed_list = sorted(processed_list, key=lambda result: float(result[3]), reverse=True)
    return processed_list


def search(query_input):
    response = {'query': str(query_input)}
    analysis = RegexTokenizer()
    tokens = analysis(query_input)
    tokens_text = [t.text for t in tokens]
    print('User input tokens', tokens)

    results_list = []
    # Results from different approaches
    results_list.extend(search_runner(tokens_text, INDEX_TITLES_EN, query_passthrough, 'H2', 5))
    results_list.extend(search_runner(tokens_text, INDEX_TITLES_EN, query2lawsyns, 'H2', 4))
    results_list.extend(search_runner(tokens_text, INDEX_TITLES_EN, query2lawsims, 'H2', 2))
    results_list.extend(search_runner(tokens_text, INDEX_TITLES_EN, query2lawsims_or, 'H2', 1))

    results_list.extend(search_runner(tokens_text, INDEX_EN, query_passthrough, 'span', 3))
    results_list.extend(search_runner(tokens_text, INDEX_EN, query2lawsyns, 'span', 2))
    results_list.extend(search_runner(tokens_text, INDEX_EN, query2lawsims, 'span', 1))
    results_list.extend(search_runner(tokens_text, INDEX_EN, query2lawsims_or, 'span', 0.5))

    # Add all the results to one list
    # results_list = titles_result_tokens + titles_result_expanded
    # results_list += text_results_tokens + text_results_expanded
    # print("Results before postprocessing: ")
    # pprint(results_list)
    # Merge duplicate results
    results_list = post_process(results_list)

    response['results'] = results_list
    return response


if __name__ == '__main__':
    from pprint import pprint

    output = input()
    tokens = output.split(' ')
    print(query2lawsims(tokens))
    # ~ pprint(output['results'])
    # pprint(output)

import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import wordnet
from whoosh import analysis
from whoosh.analysis.acore import Token
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords

stopWords = set(stopwords.words('english'))


def get_expanded_tokens(tokens):
    """Takes tokens and returns synoymns, hypernyms and lemmas"""

    def get_wordnet_pos(treebank_tag):
        """
        return WORDNET POS compliance to WORDENT lemmatization (a,n,r,v,s) 
        """
        if treebank_tag.startswith('J'):
            return wn.ADJ
        elif treebank_tag.startswith('V'):
            return wn.VERB
        elif treebank_tag.startswith('N'):
            return wn.NOUN
        elif treebank_tag.startswith('R'):
            return wn.ADV
        elif treebank_tag.startswith('S'):
            return wn.ADJ_SAT
        else:
            # As default pos in lemmatization is Noun
            return wn.NOUN

    def token_expander(term, _pos, filter_syn_level=1):
        all_terms = []
        word_syns = wn.synsets(term, pos=_pos)
        for term in word_syns:
            term_word, pos, syn_level = term.name().split('.')
            print(term_word)
            if int(syn_level) > filter_syn_level:
                continue
            all_terms.append(term_word)
            for hyper in term.hypernyms():
                hyper_word, pos, syn_level = hyper.name().split('.')
                print(hyper_word)
                if int(syn_level) > filter_syn_level:
                    continue
                all_terms.append(hyper_word)
            for lemma in term.lemmas():
                all_terms.append(lemma.name())
        return list(set(all_terms))

    pos_tokens = nltk.pos_tag(tokens)
    return [token_expander(word, get_wordnet_pos(pos)) for word, pos in pos_tokens]


class WNExpander(analysis.Filter):
    def __call__(self, tokens):
        tokens_text = [t.text for t in tokens]
        # print('tokens: ', tokens_text)
        lemmas = get_expanded_tokens(tokens_text)
        # print('lemmas: ', lemmas)
        new_tokens = []
        t = Token()
        for lemma in lemmas:
            t.text = lemma
            new_tokens.append(t)
        return new_tokens

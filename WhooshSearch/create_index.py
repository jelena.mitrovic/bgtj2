from whoosh.analysis import RegexTokenizer, StopFilter
from whoosh.index import create_in
from whoosh.fields import *

from whoosh.analysis import StemmingAnalyzer
from whoosh.fields import SchemaClass, TEXT, KEYWORD, ID, STORED

INDEX_NAME = 'en_titles'

analysis = RegexTokenizer() | StopFilter()
class MySchema(SchemaClass):
    # law_title = TEXT(analyzer=stem_ana, stored=True)
    law_title = TEXT(stored=True) #Version without the stemming analyser
    para_n = ID(stored=True)
    p_number = TEXT(stored=True)
    content = TEXT(analyzer=analysis, stored=True) # law_title
    # KEYWORDS
# Need to create a directory called indexdir for ix to be initialized.


ix = create_in(INDEX_NAME, MySchema)
DATABASE_URL = "../Databases/corpus_en.db"
TABLE_NAME = "law_title"
import sqlite3
conn = sqlite3.connect(DATABASE_URL)
c = conn.cursor()

# write_index = True
# if write_index:
writer = ix.writer()
LAWS_FROM_DB = c.execute("SELECT * from %s" % TABLE_NAME)
for i, law in enumerate(LAWS_FROM_DB):
    writer.add_document(law_title=law[0], para_n=law[1],
                        p_number=law[2], content=law[3])
    # Output structure:
    # law-title, para-#, sub-para#, law-text
writer.commit()
